<?php
function tukar_besar_kecil($string){
    $len= strlen($string);
    $output="";
    for ($i=0; $i < $len; $i++) {
    if (ctype_lower($string[$i])){
        $output .= strtoupper($string[$i]);
    }
    else {
        $output .= strtolower($string[$i]);
    }
}

    return $output;


}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"


?>